import setuptools

setuptools.setup(
    name='AWA_paper_2021',
    author='Arnau Albà',
    description='Files to reproduce plots from paper',
    url='https://gitlab.psi.ch/albajacas_a/awa_paper_2021/-/tree/master',
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    install_requires=[
        'pandas',
        'numpy',
        'h5py',
        'matplotlib',
        'scipy',
        'IPython',
    ]
)
