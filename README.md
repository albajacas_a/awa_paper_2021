# AWA paper 2021
This repository contains files for the plots and analysis in the paper ["Benchmarking Collective Effects of Electron Interactions in a Wiggler with OPAL-FEL"](https://arxiv.org/pdf/2112.02316.pdf).

# Libraries
## OPAL-FEL
To run the simulations you will need to clone and compile OPAL-FEL, which you can do by following these steps:

1- We firstly clone the OPAL/src [repository](https://gitlab.psi.ch/OPAL/src.git) and checkout the commit `fc2caa92158106a1ffa9b06364daab78c2d55cbd` (it's convenient to have an `OPAL` directory, and have the `src` repository inside it).

```
git clone https://gitlab.psi.ch/OPAL/src.git
git checkout fc2caa92158106a1ffa9b06364daab78c2d55cbd
```

2- Install all the OPAL dependencies. You can find the requirements [here](https://gitlab.psi.ch/OPAL/src/-/wikis/For%20Developers/Supported-OS-and-required-Software-to-build-OPAL), or, if you are working on the Merlin cluster at PSI, you can load the module with all the [dependencies](https://gitlab.psi.ch/OPAL/src/-/wikis/For%20Developers/Setup-build-environment-at-PSI) with
```
module load toolchain/2021.1_slurm
```

3- Additionally, OPAL-FEL uses [MITHRA 2.0](https://github.com/aryafallahi/mithra.git) as an external dependency (MIHTRA is the electromagnetic FDTD solver that OPAL-FEL uses). For this you need to clone and compile it yourself with
```
git clone https://github.com/aryafallahi/mithra.git
cd mithra
git fetch --all --tags
git checkout tags/2.0
make install
export MITHRA_PREFIX=/path/to/mithra
```

If you are working on Merlin at PSI, instead of installing MITHRA you can load the module
```
module load MITHRA/2.0_slurm
```

4- We are now ready to compile OPAL:
```
cd /path/to/OPAL/src
mkdir build
cd build
cmake -DENABLE_OPAL_FEL=yes ../
```

If everything ran successfully, you may now build OPAL with
```
make -j 4
```
where the `-j` flag gives the number of jobs.

5- It is convenient to have OPAL in the path such that it can be called from anywhere. You can do this with
```
export OPAL_EXE_PATH=/path/to/OPAL/src/build/src
export PATH=$OPAL_EXE_PATH:$PATH
```
Add these lines to your `~/.bashrc` if you want to have OPAL permanently in your path.

## Python libraries
To run the notebooks you will need to install the python libraries in `setup.py`. You can either install each library manually, or do
```
cd /path/to/awa_paper_2021
pip install .
```

Note that these are very common Python libraries, so perhaps you won't need to install any of them,

# Usage
The repository is organised in two main folders, referring to the two benchmarks in the paper.

## LCLS benchmark
In this section we reproduce the results from a [paper](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.123.214801) by MacArthur et al., where they have simulations and experimental data of an experiment at LCLS. We use OPAL-FEL to repeat the simulation and benchmark the code. The following steps explain how to do it:

1. Run all the cells in the `generate_input.ipynb` notebook. This will use the files from `plots_from_MacArthur_paper/` to generate the initial particle distribution for our simulation, and save it as `simulation/initialDistro.in`.

2. Run the OPAL simulation
```
cd simulation
sbatch run.merlin6
```

Note that the batch file `run.merlin6` is made to be run on Merlin. If you are running the simulation on another machine you will need to write your own batch file that calls `opal -np 4 LCLS.in`, or `mpirun opal LCLS.in`.

3. Now we can analyse the simulation results by running all the cells in `plot_results.ipynb`. This will generate phase-space plots from the simulation. You can play around with the notebook to plot other quantities.

## AWA benchmark
In this section we analyse and reproduce the experimental results from the AWA experiment. The Jupyter notebooks in this directory generate the input data, and analyse the experiments and simulations. You can reproduce the results from the paper by running each notebook and following the instructions in them. Note that the code in this section requires access to the AWA experiment data (link) in order to work.